# Размещение защищённого хранилища в библиотечной ОС в TEE
## 191-331: Борисенко, Мамонова
#### Инструкция по выполнению задания

При выполнении задания использовался образ Ubuntu 18.04.6 LTS (Bionic Beaver) ([ссылка на источник с образом](https://releases.ubuntu.com/18.04/ "Ubuntu 18.04.6 LTS (Bionic Beaver)")), а так же гит-репозиторий Occlum ([occlum](https://github.com/occlum/occlum "Occlum")).

----
### Шаг 1
Необходимо создать виртуальную машину с образом Ubuntu 18.04 и выполнить установку ОС.
После установки ОС необходимо открыть консоль и выполнить (если не установлен Docker) команды из инстукции по установке Docker ([установка и использование Docker в Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-ru)). 

### Шаг 2
После установки Docker нужно выполнить следующую команду:

    sudo docker run -it --env SGX_MODE=SIM occlum/occlum:latest-ubuntu20.04
    
Выполнение команды:
![sudo docker run -it --env SGX_MODE=SIM occlum/occlum:latest-ubuntu20.04](Screenshot_32.png)

### Шаг 3
После успешного выполения команды из предыдущего шага необходимо добавить в корень контейнера следующие файлы из репозитория:
- install_python.sh
- conf.yaml
- build.sh
- echo_client.py
- test.py

Для файлов install_python.sh и build.sh необходимо дать права на исполнение:

    chmod +x install_python.sh
    chmod +x build.sh

Создание файлов и выдача прав на исполнение:
![Screenshot_33.png](Screenshot_33.png)

### Шаг 4
В корне контейнера выполнить скрипт install_python.sh:

    ./install_python.sh

Скрипт выполнит установку python.
Выполнение скрипта install_python.sh:
![./install_python.sh](Screenshot_34.png)

### Шаг 5
В корне контейнера выполнить скрипт build.sh:

    ./build.sh
    
Скрипт выполнит сборку Occlum и анклава.
Выполнение скрипта build.sh:
![./build.sh(1)](Screenshot_35.png)
![./build.sh(2)](Screenshot_37.png)

### Шаг 6
В корневой директории создалась директория occlum_instance. Необходимо перейти в нее:

    cd occlum_instance/

После перехода в директорию запустить server.py внутри SGX анклава, используя occlum run:

    occlum run /bin/test.py

### Шаг 7
Открыть еще один терминал (не закрывать предыдущий с запущенным srver.py). Узнать container id используя команду:

    docker ps

Используя найденный container id открыть еще один терминал в докере:

    docker exec -it <ID контейнера> bash

### Шаг 8
Запустить client.py, используя команду:

    python3 echo_client.py

---

### Демонстрация работы
Слева на скриншоте изображена работа echo_client.py, справа - test.py. Ответ от server.py приходит в виде "Value: <значение_по_ключу или Wrong key, если ключ не найден>"
![Демонстрация работы](Screenshot_38.png)
